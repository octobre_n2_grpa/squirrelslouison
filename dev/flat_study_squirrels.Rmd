---
title: "flat_study_squirrels.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
library(glue)
library(assertthat)
library(dplyr)
library(ggplot2)

# pkgload::load_all()

# Mes tests avant de creer les fonctions

# Pour la fonction get_message_fur_color()

message("We will focus on Cinnamon squirrels")

primary_fur_color <- "Cinnamon"

message(glue("We will focus on {primary_fur_color} squirrels"))



```

<!--
 You need to run the 'description' chunk in the '0-dev_history.Rmd' file before continuing your code there.
-->

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# Get a message with a fur color
    
```{r function-get_message_fur_color}
#' Get a message with a fur color
#' 
#' You can get a message with the fur color of interest with `get_message_fur_color()`.
#' 
#' @param primary_fur_color Character. The primary fur color of interest
#' 
#' @importFrom glue glue
#' 
#' @return Used for side effect. Outputs a message in the console
#' 
#' @export
#' 
#' @examples
#' 

get_message_fur_color <- function(primary_fur_color){
  
    message(glue("We will focus on {primary_fur_color} squirrels"))
  
}
```
  
```{r example-get_message_fur_color}
get_message_fur_color(primary_fur_color = "Cinnamon")
get_message_fur_color(primary_fur_color = "Black")
get_message_fur_color(primary_fur_color = "Grey")
```
  
```{r tests-get_message_fur_color}
test_that("get_message_fur_color works", {
  expect_message(
    object = get_message_fur_color(primary_fur_color = "Cinnamon"), 
    regexp = "We will focus on Cinnamon squirrels"
  )
})

test_that("get_message_fur_color works", {
  expect_message(
    object = get_message_fur_color(primary_fur_color = "Cinnamon"), 
    regexp = "We will focus on Cinnamon squirrels"
  )
})
```
  
# Study activity
    
```{r function-study_activity}
#' Study activity
#' 
#' Cette fonction permet de tracer un graphique pour les écureuils de la couleur `col_primary_fur_color` à partir de la table `df_squirrels_act`.
#' 
#' @param df_squirrels_act dataframe. La table qui contient les écureuils de central park
#' @param col_primary_fur_color Character. The primary fur color of interest
#' 
#' @importFrom glue glue
#' @importFrom assertthat assert_that
#' @importFrom dplyr filter
#' @importFrom magrittr %>%
#' @importFrom ggplot2 ggplot aes geom_col labs scale_fill_manual
#' 
#' @return Cette fonction retourne une liste avec le tableau filtré sur une `col_primary_fur_color` et un graphique pour les écureuils de la couleur `col_primary_fur_color`
#' 
#' @export
#' 

study_activity <- function(df_squirrels_act, col_primary_fur_color) {
  
  assert_that(is.data.frame(df_squirrels_act))
  assert_that(is.character(col_primary_fur_color))
  check_squirrel_data_integrity(squirrels_data = df_squirrels_act)
  
  table <- df_squirrels_act %>% filter(primary_fur_color == col_primary_fur_color)
    
    
  graph <- table %>% 
    ggplot() +
    aes(x = activity, y = counts, fill = age) +
    geom_col() +
    labs(x = "Type of activity",
         y = "Number of observations",
         title = glue("Type of activity by age for {tolower(col_primary_fur_color)} squirrels")) +
    scale_fill_manual(name = "Age",
                      values = c("#00688B", "#00BFFF"))
  
  return(list(table = table, graph = graph))
}
```
  
```{r example-study_activity}
data(data_act_squirrels)
study_activity(
  df_squirrels_act = data_act_squirrels, 
  col_primary_fur_color = "Gray"
)
```
  
```{r tests-study_activity}
test_that("study_activity works", {
  
  data(data_act_squirrels)
  data(iris)
  
  resultat <- study_activity(df_squirrels_act = data_act_squirrels, col_primary_fur_color = "Gray")
  
  data_act_squirrels2 <- data_act_squirrels
  
  data_act_squirrels2[2][1] <- "pink"
  
  expect_true(inherits(resultat,"list"))
   
  expect_true(inherits(resultat[["table"]],"data.frame"))
  
  expect_true(inherits(resultat[["graph"]],"ggplot"))
  
  expect_error(
    object = study_activity(
      df_squirrels_act = 1, 
      col_primary_fur_color = "Gray"), 
    regexp = "df_squirrels_act is not a data frame")
  
  expect_error(
    object = study_activity(
      df_squirrels_act = iris, 
      col_primary_fur_color = 1), 
    regexp = "col_primary_fur_color is not a character vector")
  
  expect_error(
    object = study_activity(
      df_squirrels_act = iris, 
      col_primary_fur_color = "Gray"), 
    regexp = "Ne contient pas la colonne primary_fur_color")
  
  expect_error(
    object = study_activity(
      df_squirrels_act = data_act_squirrels2, 
      col_primary_fur_color = "Gray"), 
    regexp = "Il y a d'autres couleurs que Black, Gray et Cinnamon")
  
  expect_message(
    object = study_activity(
      df_squirrels_act = data_act_squirrels, 
      col_primary_fur_color = "Gray"), 
    regexp = "C'est ok !")
  
  expect_equal(
    object = resultat[["table"]],
    expected = filter(data_act_squirrels,primary_fur_color == "Gray"))
  
})
```
  
# Save as csv

Une fonction pour exporter les résultats au format .csv.

```{r function-save_as_csv}
#' Save as csv
#' 
#' Cette fonction permet de sauvegarder un tableau au format .csv.
#' 
#' @param tableau dataframe. Jeu de données que l'on souhaite sauvegarder en csv
#' @param chemin character. Chemin indiquant l'endroit où on souhaite enregistrer le fichier
#' @param ... tous les paramètres possibles de la fonction write.csv2
#' 
#' @importFrom assertthat assert_that not_empty has_extension is.writeable is.dir
#' @importFrom utils write.csv2
#' 
#' @return le chemin où le csv a été enregistré
#' 
#' @export
#' 

save_as_csv <- function(tableau, chemin, ...){
  
  assert_that(is.data.frame(tableau))
  assert_that(not_empty(tableau))
  assert_that(is.character(chemin))
  assert_that(has_extension(chemin, ext = "csv"))
  assert_that(is.dir(dirname(chemin)))
  assert_that(is.writeable(dirname(chemin)))
  
  write.csv2(x = tableau, file = chemin, ...)
  # Masque le chemin dans la console
  # return(invisible(chemin)) 
  return(normalizePath(chemin))
  
}
```
  
```{r example-save_as_csv}
# créer un chemin temporaire
temp_file <- tempfile(pattern = "savecsv")
dir.create(temp_file)

# ok : le fichier .csv est enregistré dans le projet actuel
save_as_csv(iris, paste0(temp_file, ".csv"))

save_as_csv(iris, paste0(temp_file, "2.csv"), sep = ";")

# ok : le fichier .csv est enregistré et le fichier s'ouvre
browseURL(save_as_csv(iris, paste0(temp_file, ".csv")))

# erreur : l'extension du fichier à créer n'est pas la bonne
# iris %>% save_as_csv("output.xlsx") 

# erreur : la fonction n'est pas appliquée à un data.frame
# NULL %>% save_as_csv("output.csv") 

# erreur : le chemin où doit être enregistré le fichier n'existe pas
# iris %>% save_as_csv("nexiste/pas/output.csv") 

# erreur : le chemin n'est pas accessible en écriture
# iris %>% save_as_csv("C://Windows/System/output.csv") # sur windows
```
  
```{r tests-save_as_csv}
test_that("save_as_csv works", {
  
  temp_file <- tempfile(pattern = "savecsv")
  dir.create(temp_file)
  
  save_as_csv(iris, paste0(temp_file, ".csv"))
  
  expect_true(file.exists(paste0(temp_file, ".csv")))
  
  expect_error(
      save_as_csv(iris, paste0(temp_file, ".R"))
  )
})
```
  

```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_study_squirrels.Rmd", vignette_name = "Study the squirrels")
```

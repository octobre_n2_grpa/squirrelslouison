% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/check_primary_color_is_ok.R
\name{check_primary_color_is_ok}
\alias{check_primary_color_is_ok}
\title{Check the primary color in the vector}
\usage{
check_primary_color_is_ok(string)
}
\arguments{
\item{string}{vector or character. The primary fur color a tester}
}
\value{
An error if the vector is not ok, a message if it's ok
}
\description{
You can check if the color of squirrels are Gray, Cinnamon or Black with \code{check_primary_color_is_ok()}.
}
\examples{

check_primary_color_is_ok(c("Gray","Gray","Black"))
# Renvoie un message, c'est ok !
# check_primary_color_is_ok(c("Gray","Blue","Black")) 
# Renvoie une erreur, il y a une ou des mauvaises couleurs (autre que Gray, Black, Cinnamon)
}

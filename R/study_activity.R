# WARNING - Generated by {fusen} from /dev/flat_study_squirrels.Rmd: do not edit by hand

#' Study activity
#' 
#' Cette fonction permet de tracer un graphique pour les écureuils de la couleur `col_primary_fur_color` à partir de la table `df_squirrels_act`.
#' 
#' @param df_squirrels_act dataframe. La table qui contient les écureuils de central park
#' @param col_primary_fur_color Character. The primary fur color of interest
#' 
#' @importFrom glue glue
#' @importFrom assertthat assert_that
#' @importFrom dplyr filter
#' @importFrom magrittr %>%
#' @importFrom ggplot2 ggplot aes geom_col labs scale_fill_manual
#' 
#' @return Cette fonction retourne une liste avec le tableau filtré sur une `col_primary_fur_color` et un graphique pour les écureuils de la couleur `col_primary_fur_color`
#' 
#' @export
#' 

#' @examples
#' data(data_act_squirrels)
#' study_activity(
#'   df_squirrels_act = data_act_squirrels, 
#'   col_primary_fur_color = "Gray"
#' )
study_activity <- function(df_squirrels_act, col_primary_fur_color) {
  
  assert_that(is.data.frame(df_squirrels_act))
  assert_that(is.character(col_primary_fur_color))
  check_squirrel_data_integrity(squirrels_data = df_squirrels_act)
  
  table <- df_squirrels_act %>% filter(primary_fur_color == col_primary_fur_color)
    
    
  graph <- table %>% 
    ggplot() +
    aes(x = activity, y = counts, fill = age) +
    geom_col() +
    labs(x = "Type of activity",
         y = "Number of observations",
         title = glue("Type of activity by age for {tolower(col_primary_fur_color)} squirrels")) +
    scale_fill_manual(name = "Age",
                      values = c("#00688B", "#00BFFF"))
  
  return(list(table = table, graph = graph))
}

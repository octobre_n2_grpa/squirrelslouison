# WARNING - Generated by {fusen} from /dev/flat_check_data.Rmd: do not edit by hand

#' Check squirrel data integrity
#' 
#' La fonction verifie que les donnees contiennent une colonne `primary_fur_color`, et cette colonne ne doit contenir que des couleurs autorisees.
#' 
#' @param squirrels_data Dataframe. Le jeu de donnees à tester
#' 
#' @return Une erreur si la colonne n'est pas dans le jeu de donnees. Un message si tout est ok.
#' 
#' @export
#' 
#' @examples
#' 

#' chemin_jeu_donnee <- system.file("nyc_squirrels_act_sample.csv", package = "squirrelslouison")
#' 
#' nyc_squirrels_act_sample2 <- read.csv2(file = chemin_jeu_donnee, sep = ",")
#' 
#' check_squirrel_data_integrity(squirrels_data = nyc_squirrels_act_sample2)
#' # C'est ok !
#' # check_squirrel_data_integrity(squirrels_data = nyc_squirrels_act_sample2$age)
#' # Ne fonctionne pas car la colonne n'est pas presente
check_squirrel_data_integrity <- function(squirrels_data){
    
  if (isTRUE("primary_fur_color" %in% names(squirrels_data))){
    check_primary_color_is_ok(string = squirrels_data[["primary_fur_color"]])
  }
  else{
    stop("Ne contient pas la colonne primary_fur_color")
  }
  
}

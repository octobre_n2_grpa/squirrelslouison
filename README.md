
<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrelslouison

<!-- badges: start -->
<!-- badges: end -->

Ce package sert à vérifier que la base de données contient les bonnes
couleurs d’écureuils et les bonnes coordonnées GPS dans Central Park. Il
peut également donner un message avec une couleur.

## Installation

You can install the development version of squirrelslouison like so:

``` r
remotes::install_local(path = "~/squirrels_0.0.0.9000.tar.gz") # installe le package depuis le format tar.gz
```
